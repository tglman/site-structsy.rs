---
title : Structsy 0.3 Release
layout: default.liquid
---

After a bit more the half year here is a new release of Structsy, this bring a big set of new features, fixes and updates.

First thing this release use the latest version of [Persy](http://www.persy.rs) with all the related improvements.

In terms of Rust structures support from this release [enums](persistence.html#enums) are supported, it is possible to persist enums by themselves or embedded
in other enums or structs, simple variants, or variants with a single embedded value are supported. 

For the query part now is possible to add [ordering](queries.html#orders) in the filters, enums with simple variants can be also used in the filters,
also has been fixed the filtering of embedded structs when filtering data considering the transaction changes.
In addition of the filter in the queries now is possible to use simple [projections](queries.html#projections) to return only the fields that you want.

In terms of the database management there have been two major changes:
- the data migration now allow to migrate any struct that has a supported type also structs that contain references, example post will come soon.
- since the upgrade to more recent Persy  is now possible to have in memory only structsy instances, useful for tests or caching.


