---
title : Structsy 0.2 Release
layout: default.liquid
---

Structsy 0.2 is the first release that I can consider good enough to let other people start to play with, it introduce most of the basic concepts needed
and can solve a quite wide range of persistence problems, so what are the features included?   
  
This release include the basic peristence of structs with basic types or with embeedded structs simply using the `#[derive(Persistent)]` and `#[derive(PersistentEmbedded)]`,
this was already included in Structsy 0.1 even though no public post where made, you can check [What's Structsy](/) to see all the details.

The new feature introduced by this release is the query engine that allow to do complex queries on top of persistent structs, using simple Rust traits
and methods, you can check the details on how write queries in [Here](queries.html).  
  
For now on I will collecting feedbacks to see if this approach to persistence is appreciated and maybe push this libraries to support a more wide set of use cases
at this moment there are some big missing features that will be really interesting to see supported in future, like support of persistence of Rust enums, 
and ability to define a projection with a subset of fields of the persistent struct.

That's all!!
