---
title : Structsy 0.5 Release
layout: default.liquid
---

Has been a long while since the last release of Structsy, this release has been a long work, more or less the biggest release to date, 
even though it does not include new APIs, most of the work was done to rewrite the query engine.

### Changes

As said this release include only one big change, a new query engine re-written from scratch and plugged-in, this does not 
break any public APIs, it actually allows to fix some use cases where the transaction context was not used when filtering on referred records,
as well this prepares for future improvements in queries, like use of indexes in more cases, query planning based on statistics and advanced query operations like use functions or grouping.

### Future

The next steps for this project did not change much in the last year or so, the main target is to find a nice way to describe
most query concepts in pure rust, like join query, complex projections and grouping, and now that there is a new query engine
plugged in it is possible to start to work on this more complex features. 

