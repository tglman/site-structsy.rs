---
title : Index Support
layout: default.liquid
---

Structsy has basic transparent index support, when a struct is defined is possible to define and index on one of it's fields, for example:

```rust
#[derive(Persistent)]
struct Contact {
	#[index(mode="exclusive")]
	id:u32,
	#[index(mode="cluster")]
	name: String,
	description:String,
}
```
In this case I'm declaring a struct with two fields indexed the `id` in `exclusive` mode and the `name` in `cluster` mode, before digging on the details
of what means each indexing mode, let me clear that this is the only things is needed for indexing, all the rest of structsy structures will adapt automatically 
to the index the insert and update will automatically update the index together with the data, and the query engine will use the index where is possible, even trying the prefer it on the full scan of the records,
as structsy 0.2 version adding or removing of an index will need full migration of the persistent data.  
  

Now let's dig in the details of the index mode, structsy use the same indexing capabilities of the underlying persy, so the index modes that exists are 3
`exclusive`, `cluster` and `replace`.  
**exclusive** means that only one record at time can be associate with a value, any subsequent association will fail together with whole transaction, this correspond to the unique key behaviour of traditional databases.  
**cluster** means that multiple record can be associated to a value, this is the classic example of indexing for improve query performances.   
**replace** It works in a similar way to exclusive, but instead of fail the value will be associated only to the last inserted value, this has not a lot of meaning in this context, is just and indexing approach of persy exposed to the higher level, this may be removed in structsy in future.  

That's all you need to know about indexing in structsy!!  
  
  
