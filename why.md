---
title : Why
layout: default.liquid
---
I've been using rust for a while now, and I didn't find a way to handle persistent data that satisfy me completely, 
so watching the implementation of some mocking libraries (like mockiato) I got the inspiration to attempt to define a different way to handle persistent data
and the access of it.  
  
Together with this I've been working for a while building a simple single file transactional storage called [Persy](https://persy.rs), so for me was the natural approach to use my persistency
library to persist the data while trying to define this new approach, to be able to verify the usability of persy and also build something that could reach a 
wider audience.

This project even though is a embeddable database and probably will remain for ever an embeddable database, will probably evolve to a bit more complex beast,
I'm hoping that once I validate the soundness of this approach I can extract some peaces and spin off some other projects using the same concepts,
for example write a ORM (or should I say SRM = Struct Relational Mapping) that use all basic rust types without any specific syntax or languages.

Time will tell!!!


