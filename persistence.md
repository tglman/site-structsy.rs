---
title : Persistence
layout: default.liquid
---

Structsy support the persistence of rust structs and enums.  

## Structs

for make possible to persist a struct is sufficient to derive `Persistent`.

```rust
use structsy_derive::Persistent;

#[derive(Persistent)]
struct Example {
    name:String,
}

```

## <a id="enums"></a> Enums

In the same way of struct is possible to make an enum persistent just by adding the derive `Persistent`

```rust 
#[derive(Persistent)]
enum EnumExample {
    First,
    Second(i32),
}

```
is supported the persistence of enums with simple variants or with a variant with a single value, either simple or embedded struct or enum.


## Basic types

Most of the basic types are handled directly by structsy, for example this can be persisted correctly.

```rust
use structsy_derive::Persistent;

#[derive(Persistent)]
struct Example {
	field_u8:u8,
	field_u16:u16,
	field_u32:u32,
	field_u64:u64,
	field_u128:u128,
	field_i8:i8,
	field_i16:i16,
	field_i32:i32,
	field_i64:i64,
	field_i128:i128,
	field_f32:f32,
	field_f64:f64,
	desc :String,
	reference_id:Ref<OtherStruct>,
	// All basic tipes are supported as value of the vector
	vec:Vec<u8>,
	// All basic tipes are supported as value of the option
	option:Option<u8>,
}

```

## Embedded

if you need to persist a struct or a enum as a field of a struct, you need to derive `PersistentEmbedded` as example

```rust
use structsy_derive::{Persistent, PersistentEmbedded};

#[derive(Persistent)]
struct Example {
    st_emb: Embedded,
    en_emb: EnumEmbedded,
}

#[derive(Persistent)]
enum ExampleEnum {
    First(Embedded),
    Second(EnumEmbedded),
}

#[derive(PersistentEmbedded)]
struct Embedded {
    name:String,
}

#[derive(PersistentEmbedded)]
enum EnumEmbedded {
    First,
    Second
}
```

This is all the possible persistence options available in structsy.


