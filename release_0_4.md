---
title : Structsy 0.4 Release
layout: default.liquid
---

After a bit more the half year here is a new release of Structsy, this brings a small set of new features and 
some underlying structure stabilization, in detail:

### Updates

First, this release update to [Persy 1.0](http://www.persy.rs) this means that there is a big sets of features
that are getting pretty stable, mainly in terms of durability, disk space management, and open to the possibility
to have at some point backward disk compatibility even though is not yet done in Structsy, 
this Persy update also guarantee a good support of concurrent transactions.

### New Features
In Structsy itself there has been the inclusion of a few new features, namely a new "Snapshot" API,  a "debug" record API, and some minor renaming and cleanups.

##### Snapshots
The snapshots allow to do read operations stopped in time, ignoring any subsequent write operations, this is useful for backup and queries that may not 
include the latest data of the latest transaction, but have a consistent state at the last transaction considered by the 
snapshot, this differs with a normal read operation that works with read committed, that may include partial data of a new 
transaction committed while query is executed.

##### Debug Rercord API
The debug record API, allow read and write data in Structsy databases, without the need of the
original rust code that defined the structures, the target of this API is to be used to build generic export import tools
and data inspection tools.


### Next
All the main APIs of Structsy are already defined, there are a few use cases not covered by Structsy, like grouping/reducing operations
and joining structs in a query on not predefined connections, that are so because I could not find a nice enough design for them, so I'm not planning 
to include them in the first major version at least if I not find a solution in the meanwhile, all the work on Structsy
will be going around stabilization, refactor of internal parts that are not good enough, and trying to introduce some secondary
features like statistics for query optimizations, done this I can think to release the first major version of Structsy,
I do expect though one or two minors more, and some improvements on tooling and examples.

